import numpy as np
import sklearn
from sklearn import datasets
import tensorflow as tf
from tensorflow import keras
import rbflayer
import kmeans_initializer
from matplotlib import pyplot as plt
from keras.datasets import boston_housing


def tensor_sum(tensor):
    return tf.reduce_sum(tensor, 1)

(dataset_inputs_training, dataset_target_training), (dataset_inputs_validation, dataset_target_validation) = boston_housing.load_data(test_split=0.25)

dataset_inputs_training = (dataset_inputs_training - np.mean(dataset_inputs_training, axis=0)) / np.std(dataset_inputs_training, axis=0)
dataset_inputs_validation = (dataset_inputs_validation - np.mean(dataset_inputs_validation, axis=0)) / np.std(dataset_inputs_validation, axis=0)


num_inputs = min(dataset_inputs_training.shape)
num_dense_outputs = 128
num_outputs = 1
learning_rate = 0.001
epochs = 100
fraction_of_examples = [0.1,0.5,0.9]



def R_squared(y_true, y_pred):

    SS_res = tf.math.reduce_sum(tf.math.squared_difference(y_true, y_pred))
    SS_total = tf.math.reduce_sum(tf.math.squared_difference(y_true, tf.math.reduce_mean(y_true)))
    Rsquare = 1 - SS_res/SS_total
    return Rsquare

def model_with_n_neurons(percentage):

    P = int(np.round(percentage*np.max(dataset_inputs_training.shape)))  # number of RBF neurons
    centers = sklearn.cluster.KMeans(n_clusters=P).fit(dataset_inputs_training).cluster_centers_
    dMax = max([np.linalg.norm(c1 - c2) for c1 in centers for c2 in centers])
    sigma = np.repeat(dMax / np.sqrt(2*P), P)
    
    RBF_layer = rbflayer.RBFLayer(P, initializer=tf.constant_initializer(value=centers),
                            betas=sigma,
                            input_shape=(13,))
    model = keras.Sequential()
    model.add(RBF_layer)
    model.add(keras.layers.Dense(num_dense_outputs))
    model.add(keras.layers.Dense(1))
    model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=learning_rate),
                loss=tf.keras.losses.mean_squared_error,
                metrics=[tf.keras.metrics.RootMeanSquaredError(),
                        R_squared])
    history = model.fit(dataset_inputs_training, dataset_target_training,
                        epochs=epochs,batch_size=32,
                        validation_data=(dataset_inputs_validation, dataset_target_validation),)

    return model,history

for i in range(3):
    model, history = model_with_n_neurons(fraction_of_examples[i])
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.grid()
    plt.title('Training curves ' + str(i))
    plt.legend(['training loss', 'validation loss'])
    plt.savefig('training_curves_'+str(i))
    # plt.show()
    plt.clf()

    plt.plot(history.history['root_mean_squared_error'])
    plt.plot(history.history['val_root_mean_squared_error'])
    plt.grid()
    plt.title('RMSE ' + str(i))
    plt.legend(['training RMSE','validation RMSE'])
    plt.savefig('RMSE_'+str(i))
    # plt.show()
    plt.clf()


    plt.plot(history.history['R_squared'])
    plt.plot(history.history['val_R_squared'])
    plt.grid()
    plt.title('R squared ' + str(i))
    plt.legend(['training R_squared','validation R_squared'])
    plt.savefig('R_square_'+str(i))
    # plt.show()
    plt.clf()


