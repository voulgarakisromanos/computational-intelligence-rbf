import numpy as np
import sklearn
from sklearn import datasets
import tensorflow as tf
from tensorflow import keras
import rbflayer
import kmeans_initializer
from matplotlib import pyplot as plt
from keras.datasets import boston_housing
from tensorflow import keras
import keras_tuner as kt


def tensor_sum(tensor):
    return tf.reduce_sum(tensor, 1)

(dataset_inputs_training, dataset_target_training), (dataset_inputs_validation, dataset_target_validation) = boston_housing.load_data(test_split=0.25)

dataset_inputs_training = (dataset_inputs_training - np.mean(dataset_inputs_training, axis=0)) / np.std(dataset_inputs_training, axis=0)
dataset_inputs_validation = (dataset_inputs_validation - np.mean(dataset_inputs_validation, axis=0)) / np.std(dataset_inputs_validation, axis=0)

def R_squared(y_true, y_pred):

    SS_res = tf.math.reduce_sum(tf.math.squared_difference(y_true, y_pred))
    SS_total = tf.math.reduce_sum(tf.math.squared_difference(y_true, tf.math.reduce_mean(y_true)))
    Rsquare = 1 - SS_res/SS_total
    return Rsquare

def build_model(hp):
    dataset_training_size = 379
    RBF_neurons = hp.Choice('RBF_neurons', [int(dataset_training_size*percentage) for percentage in [0.05, 0.15, 0.3, 0.5]])
    linear_neurons = hp.Choice('linear_neurons', [32, 64, 128, 256])
    dropout_chance = hp.Choice('dropout_chance', [0.2, 0.35, 0.5])
    learning_rate = 0.001
    epochs = 100

    centers = sklearn.cluster.KMeans(n_clusters=RBF_neurons).fit(dataset_inputs_training).cluster_centers_
    dMax = max([np.linalg.norm(c1 - c2) for c1 in centers for c2 in centers])
    sigma = np.repeat(dMax / np.sqrt(2*RBF_neurons), RBF_neurons)


    RBF_layer = rbflayer.RBFLayer(RBF_neurons, initializer=tf.constant_initializer(value=centers),
                                input_shape=(13,))
    model = keras.Sequential()
    model.add(RBF_layer)
    model.add(keras.layers.Dense(linear_neurons))
    model.add(keras.layers.Dropout(rate=dropout_chance))
    model.add(keras.layers.Dense(1))
    model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=learning_rate),
                loss=tf.keras.losses.mean_squared_error,
                metrics=[tf.keras.metrics.RootMeanSquaredError(),
                        R_squared])

    return model

possible_RBF_neurons = [0.05,0.15,0.3,0,5]

tuner = kt.RandomSearch(
    build_model,
    objective='val_loss',
    max_trials=50)


stop_early = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)

tuner.search(dataset_inputs_training, dataset_target_training, epochs=100, validation_data=(dataset_inputs_validation, dataset_target_validation), callbacks=[stop_early])
best_model = tuner.get_best_models()[0]
best_hyperparameters = tuner.get_best_hyperparameters()[0]

predicted = best_model.predict(dataset_inputs_validation)

print(R_squared(predicted.reshape((-1,)),dataset_target_validation))

# best_parameters = RBF_neurons 189, linear_neurons 128, dropout 0.35
